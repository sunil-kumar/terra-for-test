#EFS Creation
resource "aws_efs_file_system" "myefs" {
   creation_token = "myefs"
   performance_mode = var.performance_mode
   throughput_mode = var.throughput_mode
   encrypted = var.encryptionmode
 tags = {
     Name = "${var.efstag}-${var.project-name}"
   }
 }

# Security Group Creation
resource "aws_security_group" "mysec_grp_nfs" {
  name        = "${var.nfs_security_group_name}-${var.project-name}"
  description = "test_sec_grp1"
  vpc_id      = var.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = var.ingress_nfs
    to_port     = var.ingress_nfs
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = var.egress_nfs
    to_port     = var.egress_nfs
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test_sec_grp1-${var.project-name}"
  }
 }
#Mount-point Creation
resource "aws_efs_mount_target" "myefs-mt" {
   file_system_id  = aws_efs_file_system.myefs.id
   subnet_id = var.subnet1_id
   security_groups = [aws_security_group.mysec_grp_nfs.id]
}


#outputs
output "efs_id" {
  value = aws_efs_file_system.myefs.id
}
output "efs_mt_id" {
  value = aws_efs_mount_target.myefs-mt
}
output "nfs_security_group" {
  value = aws_security_group.mysec_grp_nfs.id
}

