variable "performance_mode" {
  default = "generalPurpose"
}
variable "throughput_mode" {
  default = "bursting"
}
variable "encryptionmode" {
  default = "true"
}
variable "efstag" {

}
variable "nfs_security_group_name" {

}
variable "ingress_nfs" {

}
variable "egress_nfs" {

}
variable "vpc_id" {

}
variable "subnet1_id" {

}
variable "project-name" {

}
