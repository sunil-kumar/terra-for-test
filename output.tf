#VPC Module Outputs
output "vpc_id" {
  description = "ID of project VPC"
  value       = module.vpc.vpc_id
}
output "subnet1_id" {
  description = "ID of project Subnet1"
  value       = module.vpc.subnet1_id
}
output "subnet2_id" {
  description = "ID of project Subnet2"
  value       = module.vpc.subnet2_id
}
output "routetable_id" {
  description = "ID of project Routetable"
  value       = module.vpc.route_table_id
}
output "vpc_security_group_id" {
  description = "ID of project VPC_SecurityGroup"
  value       = module.vpc.security_group_vpc
}
#S3 module Outputs
output "s3_bucket_id" {
  description = "ID of project S3_bucket"
  value       = module.s3.bucket_id
}
#EFS module Outputs
output "efs_id" {
  description = "ID of project EFS"
  value       = module.efs.efs_id
}
output "efs_mount_id" {
  description = "ID of project EFS_MOUNT"
  value       = module.efs.efs_mt_id
}
output "nfs_security_group_id" {
  description = "ID of project NFS Security Group"
  value       = module.efs.nfs_security_group
}
#VPN Module outputs
output "customer_gateway_id" {
  description = "ID of project Customer Gateway"
  value       = module.vpn.customer_gateway_id
}
output "vpn_gateway_id" {
  description = "ID of project VPN Gateway"
  value       = module.vpn.vpn_gateway_id
}
output "vpn_connection_id" {
  description = "ID of project VPN Connect"
  value       = module.vpn.vpn_connection_id
}
output "vpn_connection_route_id" {
  description = "ID of project VPN connection Route"
  value       = module.vpn.vpn_connection_route_id
}
output "vpn_connection_route_propagation_id" {
  description = "ID of project VPN connection Route Propagation"
  value       = module.vpn.vpn_connection_route_propagation_id
}
#ASG Module outputs
output "Lanuch_config_securitygroup_id" {
  description = "ID of project Lanuch_config_securitygroup"
  value       = module.asg.Lanuch_config_securitygroup_id
}
output "Lanuch_configuration_id" {
  description = "ID of project Lanuch_configuration"
  value       = module.asg.Lanuch_configuration_id
}
output "ELB_securitygroup_id" {
  description = "ID of project ELB_securitygroup"
  value       = module.asg.ELB_securitygroup_id
}
output "ELB_id" {
  description = "ID of project ELB"
  value       = module.asg.ELB_id
}
output "Autoscaling_group_id" {
  description = "ID of project Autoscaling_group"
  value       = module.asg.Autoscaling_group_id
}
