#Customer gateway Creation
resource "aws_customer_gateway" "test_customer_gateway" {
  bgp_asn    = var.customer-gateway-bgp_asn
  ip_address = var.customer-gateway-ip_address
  type       = var.customer-gateway-type

  tags = {
    Name = "${var.customer-gateway-tag-name}-${var.project-name}"
  }
}

#Creating VPN gateway
resource "aws_vpn_gateway" "test_vpn_gw" {
  vpc_id = var.vpc_id

  tags = {
    Name = "${var.vpn-tag-name}-${var.project-name}"
  }
}

#Creating VPN Connection
resource "aws_vpn_connection" "test-vpn-connection" {
  vpn_gateway_id      = aws_vpn_gateway.test_vpn_gw.id
  customer_gateway_id = aws_customer_gateway.test_customer_gateway.id
  type                = var.vpn-connection-type
  static_routes_only  = true
}

#Creating VPN Connection route
resource "aws_vpn_connection_route" "test-connection-route" {
  destination_cidr_block = var.vpn-connection-route-destination_cidr_block
  vpn_connection_id      = aws_vpn_connection.test-vpn-connection.id
}

#Automatic Routetable propagatoin
resource "aws_vpn_gateway_route_propagation" "example" {
  vpn_gateway_id = aws_vpn_gateway.test_vpn_gw.id
  route_table_id = var.route_table_id
}

#output
output "customer_gateway_id" {
  value = aws_customer_gateway.test_customer_gateway.id
}
output "vpn_gateway_id" {
  value = aws_vpn_gateway.test_vpn_gw.id
}
output "vpn_connection_id" {
  value = aws_vpn_connection.test-vpn-connection.id
}
output "vpn_connection_route_id" {
  value = aws_vpn_connection_route.test-connection-route.id
}
output "vpn_connection_route_propagation_id" {
  value = aws_vpn_gateway_route_propagation.example.id
}
