variable "customer-gateway-bgp_asn" {
  default = "65000"
}
variable "customer-gateway-ip_address" {
  default = "172.0.0.1"
}
variable "customer-gateway-type" {
  default = "ipsec.1"
}
variable "customer-gateway-tag-name" {
  default = "main-customer-gateway"
}
variable "vpc_id" {

}
variable "vpn-tag-name" {
  default = "main-vpn-test"
}
variable "vpn-connection-type" {
  default = "ipsec.1"
}
variable "vpn-connection-route-destination_cidr_block" {
  default = "10.10.2.0/24"
}
variable "route_table_id" {

}
variable "project-name" {

}
