terraform {
  backend "s3" {
    bucket = "sunil-privatebuckettest"
    key    = "terraform/state"
    region = "ap-south-1"
  }
}
