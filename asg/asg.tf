#Security Group for Lanuch configuration
resource "aws_security_group" "lc_security_group" {
  name        = "${var.lcsg_name}-${var.project-name}"
  vpc_id = var.vpc_id

  ingress {
    from_port   = var.ingress_lcsg_from_http_port
    to_port     = var.ingress_lcsg_to_http_port
    protocol    = var.ingress_lcsg_protocal
    cidr_blocks = [var.ingress_lcsg_cidr_block]
  }

  egress {
    from_port       = var.egress_lcsg_from_http_port
    to_port         = var.egress_lcsg_to_http_port
    protocol        = "-1"
    cidr_blocks     = [var.egress_lcsg_cidr_block]
  }

  tags = {
    Name = "${var.lcsg_tag_name}-${var.project-name}"
  }
}

#Lanuch Configuration
resource "aws_launch_configuration" "web" {
  name_prefix = "${var.lcname}-${var.project-name}"
  image_id = var.image_id
  instance_type = var.instance_type
  key_name = var.keypair_name
  security_groups = ["${aws_security_group.lc_security_group.id}"]
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }
}

#Security group creation for ELB
resource "aws_security_group" "elb_http" {
  name        = "${var.elb_sg_name}-${var.project-name}"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"
  vpc_id = var.vpc_id

  ingress {
    from_port   = var.ingress_elb_from_http_port
    to_port     = var.ingress_elb_to_http_port
    protocol    = var.ingress_protocal
    cidr_blocks = [var.ingress_elb_cidr_block]
  }

  egress {
    from_port       = var.egress_elb_from_http_port
    to_port         = var.egress_elb_to_http_port
    protocol        = "-1"
    cidr_blocks     = [var.egress_elb_cidr_block]
  }

  tags = {
    Name = "${var.elb_sg_tag_name}-${var.project-name}"
  }
}

resource "aws_elb" "web_elb" {
  name = "${var.elb_name}-${var.project-name}"
  internal = true
  security_groups = ["${aws_security_group.elb_http.id}"]
  subnets = [
    var.subnet1_id ,
        var.subnet2_id
  ]

  cross_zone_load_balancing   = true

  health_check {
    healthy_threshold = var.healthy_threshold
    unhealthy_threshold = var.unhealthy_threshold
    timeout = var.health_check_timeout
    interval = var.health_check_interval
    target = var.target_protocal
  }

  listener {
    lb_port = var.listener_lb_port
    lb_protocol = var.listen_lb_protocal
    instance_port = var.listener_instance_port
    instance_protocol = var.listener_instance_portocal
  }

}

#Creating Autoscaling Group
resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-${var.project-name}"

  min_size             = var.asg_min_size
  desired_capacity     = var.asg_desired_capacity
  max_size             = var.asg_max_size

  health_check_type    = "ELB"
  load_balancers = [
    aws_elb.web_elb.id
  ]

  launch_configuration = aws_launch_configuration.web.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  vpc_zone_identifier  = [
    var.subnet1_id ,
        var.subnet2_id
  ]

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "${var.asg_key_name}"
    value               = "${var.asg_key_value}-${var.project-name}"
    propagate_at_launch = true
  }

}
#Output
output "Lanuch_config_securitygroup_id" {
  value = aws_security_group.lc_security_group.id
}
output "Lanuch_configuration_id" {
  value = aws_launch_configuration.web.name
}
output "ELB_securitygroup_id" {
  value = aws_security_group.elb_http.id
}
output "ELB_id" {
  value = aws_elb.web_elb.id
}
output "Autoscaling_group_id" {
  value = aws_autoscaling_group.web.id
}
