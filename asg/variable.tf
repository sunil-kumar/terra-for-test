#variable for lanch configuration Security group
variable "lcsg_name" {
  default = "test-lc-sg"
}
variable "vpc_id" {

}
variable "ingress_lcsg_from_http_port" {
  default = "0"
}
variable "ingress_lcsg_to_http_port" {
  default = "0"
}
variable "ingress_lcsg_protocal" {
  default = "tcp"
}
variable "ingress_lcsg_cidr_block" {
  default = "0.0.0.0/0"
}

variable "egress_lcsg_from_http_port" {
  default = "0"
}
variable "egress_lcsg_to_http_port" {
  default = "0"
}
variable "egress_lcsg_cidr_block" {
  default = "0.0.0.0/0"
}
variable "lcsg_tag_name" {
  default = "test-lc-sg-tag"
}

#Variable for Lanch configuration
variable "lcname" {
  default = "testlc"
}
variable "image_id" {

}
variable "instance_type" {
  default = "m4.large"
}
variable "keypair_name" {
  default = "test-key"
}

#variable for ELB Security group
variable "elb_sg_name" {
  default = "test-elb-sg"
}
variable "ingress_elb_from_http_port" {
  default = "80"
}
variable "ingress_elb_to_http_port" {
  default = "80"
}
variable "ingress_protocal" {
  default = "TCP"
}
variable "ingress_elb_cidr_block" {
  default = "0.0.0.0/0"
}
variable "egress_elb_from_http_port" {
  default = "0"
}
variable "egress_elb_to_http_port" {
  default = "0"
}
variable "egress_elb_cidr_block" {
  default = "0.0.0.0/0"
}
variable "elb_sg_tag_name" {
  default = "test-elb-sg-tag"
}
variable "elb_name" {
  default = "testweb-elb"
}
variable "subnet1_id" {

}
variable "subnet2_id" {

}
variable "healthy_threshold" {
  default = "7"
}
variable "unhealthy_threshold" {
  default = "5"
}
variable "health_check_timeout" {
  default = "10"
}
variable "health_check_interval" {
  default = "30"
}
variable "target_protocal" {
  default = "HTTP:80/"
}
variable "listener_lb_port" {
  default = "80"
}
variable "listen_lb_protocal" {
  default = "HTTP"
}
variable "listener_instance_port" {
  default = "80"
}
variable "listener_instance_portocal" {
  default = "HTTP"
}
variable "asg_min_size" {
  default = "2"
}
variable "asg_desired_capacity" {
  default = "2"
}
variable "asg_max_size" {
  default = "6"
}
variable "asg_key_name" {
  default = "Name"
}
variable "asg_key_value" {
  default = "Test-value"
}
variable "project-name" {

}
