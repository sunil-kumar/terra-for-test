variable "region" {
  default = "ap-south-1"
}
#VPCMODULE
variable "VPCCIDR" {
  default = "10.10.0.0/16"
}
variable "vpcname" {
  default = "privatevpc"
}
variable "Subnet1CIDR" {
  default = "10.10.1.0/24"
}
variable "subnet1Az" {
  default = "aps1-az1"
}
variable "Subnet1name"{
  default = "privatesubnet1"
}
variable "Subnet2CIDR" {
  default = "10.10.2.0/24"
}
variable "subnet2Az" {
  default = "aps1-az2"
}
variable "Subnet2name"{
  default = "privatesubnet2"
}
variable "Routetablename" {
  default = "privateroutetable"
}
variable "vpc_securitygroup_name" {
  default = "vpc-sg"
}
variable "ingress_vpc" {
  default = "22"
}
variable "egress_vpc" {
  default = "0"
}
#S3MODULE
variable "bucketname" {
  default = "testmybucketapsoutkaler"
}
variable "acl" {
 default = "private"
}
variable "keyname" {
 default = "my-bucket"
}
variable "keyvalue" {
 default = "production"
}
#EFSMODULE
variable "performance_mode" {
  default = "generalPurpose"
}
variable "throughput_mode" {
  default = "bursting"
}
variable "encryptionmode" {
  default = "true"
}
variable "efstag" {
  default = "testefs"
}
variable "nfs_security_group_name" {
  default = "nfs-sg"
}
variable "ingress_nfs" {
  default = "2049"
}
variable "egress_nfs" {
  default = "0"
}
variable "customer-gateway-bgp_asn" {
 default = "65000"
}
variable "customer-gateway-ip_address" {
 default = "172.0.0.1"
}
variable "customer-gateway-type" {
 default = "ipsec.1"
}
variable "customer-gateway-tag-name" {
 default = "main-customer-gateway"
}
variable "vpn-tag-name" {
 default = "main-vpn-test"
}
variable "vpn-connection-type" {
 default = "ipsec.1"
}
variable "vpn-connection-route-destination_cidr_block" {
 default = "10.10.2.0/24"
}
#variable for lanch configuration Security group
variable "lcsg_name" {
  default = "test-lc-sg"
}
variable "ingress_lcsg_from_http_port" {
  default = "0"
}
variable "ingress_lcsg_to_http_port" {
  default = "0"
}
variable "ingress_lcsg_protocal" {
  default = "tcp"
}
variable "ingress_lcsg_cidr_block" {
  default = "0.0.0.0/0"
}

variable "egress_lcsg_from_http_port" {
  default = "0"
}
variable "egress_lcsg_to_http_port" {
  default = "0"
}
variable "egress_lcsg_cidr_block" {
  default = "0.0.0.0/0"
}
variable "lcsg_tag_name" {
  default = "test-lc-sg-tag"
}

#Variable for Lanch configuration
variable "lcname" {
  default = "testlc"
}
variable "image_id" {
  default = "ami-0f1fb91a596abf28d"
}
variable "instance_type" {
  default = "m4.large"
}
variable "keypair_name" {
  default = "test-key"
}

#variable for ELB Security group
variable "elb_sg_name" {
  default = "test-elb-sg"
}
variable "ingress_elb_from_http_port" {
  default = "80"
}
variable "ingress_elb_to_http_port" {
  default = "80"
}
variable "ingress_protocal" {
  default = "TCP"
}
variable "ingress_elb_cidr_block" {
  default = "0.0.0.0/0"
}

variable "egress_elb_from_http_port" {
  default = "0"
}
variable "egress_elb_to_http_port" {
  default = "0"
}
variable "egress_elb_cidr_block" {
  default = "0.0.0.0/0"
}
variable "elb_sg_tag_name" {
  default = "test-elb-sg-tag"
}
variable "elb_name" {
  default = "testweb-elb"
}
variable "healthy_threshold" {
  default = "7"
}
variable "unhealthy_threshold" {
  default = "5"
}
variable "health_check_timeout" {
  default = "10"
}
variable "health_check_interval" {
  default = "30"
}
variable "target_protocal" {
  default = "HTTP:80/"
}
variable "listener_lb_port" {
  default = "80"
}
variable "listen_lb_protocal" {
  default = "HTTP"
}
variable "listener_instance_port" {
  default = "80"
}
variable "listener_instance_portocal" {
  default = "HTTP"
}
variable "asg_min_size" {
  default = "2"
}
variable "asg_desired_capacity" {
  default = "2"
}
variable "asg_max_size" {
  default = "6"
}
variable "asg_key_name" {
  default = "Name"
}
variable "asg_key_value" {
  default = "Test-value"
}
variable "project-name" {
  default = "Test-project"
}
