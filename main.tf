provider "aws" {
  region  = var.region
}

module "vpc" {
  source = "./vpc"
  project-name = var.project-name
  VPCCIDR= var.VPCCIDR
  vpcname = var.vpcname
  Subnet1CIDR = var.Subnet1CIDR
  subnet1Az = var.subnet1Az
  Subnet1name = var.Subnet1name
  Subnet2CIDR = var.Subnet2CIDR
  subnet2Az = var.subnet2Az
  Subnet2name = var.Subnet2name
  Routetablename = var.Routetablename
  vpc_securitygroup_name = var.vpc_securitygroup_name
  ingress_vpc = var.ingress_vpc
  egress_vpc = var.egress_vpc
  }

module "s3" {
  source = "./s3"
  project-name = var.project-name
  bucketname = var.bucketname
  acl = var.acl
  keyname = var.keyname
  keyvalue = var.keyvalue
  }

module "efs" {
  source = "./efs"
  project-name = var.project-name
  performance_mode = var.performance_mode
  throughput_mode = var.throughput_mode
  encryptionmode = var.encryptionmode
  efstag = var.efstag
  vpc_id = module.vpc.vpc_id
  subnet1_id = module.vpc.subnet1_id
  nfs_security_group_name = var.nfs_security_group_name
  ingress_nfs = var.ingress_nfs
  egress_nfs = var.egress_nfs
  }

module "vpn" {
  source = "./vpn"
  project-name = var.project-name
  customer-gateway-bgp_asn = var.customer-gateway-bgp_asn
  customer-gateway-ip_address = var.customer-gateway-ip_address
  customer-gateway-type = var.customer-gateway-type
  customer-gateway-tag-name = var.customer-gateway-tag-name
  vpc_id = module.vpc.vpc_id
  vpn-tag-name = var.vpn-tag-name
  vpn-connection-type = var.vpn-connection-type
  vpn-connection-route-destination_cidr_block = var.vpn-connection-route-destination_cidr_block
  route_table_id = module.vpc.route_table_id
  }

module "asg" {
  source = "./asg"
  project-name = var.project-name
  lcsg_name = var.lcsg_name
  ingress_lcsg_from_http_port = var.ingress_lcsg_from_http_port
  ingress_lcsg_to_http_port = var.ingress_lcsg_to_http_port
  ingress_lcsg_protocal = var.ingress_lcsg_protocal
  ingress_lcsg_cidr_block = var.ingress_lcsg_cidr_block
  egress_lcsg_from_http_port = var.egress_lcsg_from_http_port
  egress_lcsg_to_http_port = var.egress_lcsg_to_http_port
  egress_lcsg_cidr_block =  var.egress_lcsg_cidr_block
  lcsg_tag_name = var.lcsg_tag_name
  lcname = var.lcname
  image_id = var.image_id
  instance_type = var.instance_type
  keypair_name = var.keypair_name
  elb_sg_name = var.elb_sg_name
  vpc_id = module.vpc.vpc_id
  ingress_elb_from_http_port = var.ingress_elb_from_http_port
  ingress_elb_to_http_port = var.ingress_elb_to_http_port
  ingress_protocal = var.ingress_protocal
  ingress_elb_cidr_block = var.ingress_elb_cidr_block
  egress_elb_from_http_port = var.egress_elb_from_http_port
  egress_elb_to_http_port =  var.egress_elb_to_http_port
  egress_elb_cidr_block = var.egress_elb_cidr_block
  elb_sg_tag_name = var.elb_sg_tag_name
  elb_name = var.elb_name
  subnet1_id = module.vpc.subnet1_id
  subnet2_id = module.vpc.subnet2_id
  healthy_threshold = var.healthy_threshold
  unhealthy_threshold = var.unhealthy_threshold
  health_check_timeout = var.health_check_timeout
  health_check_interval = var.health_check_interval
  target_protocal = var.target_protocal
  listener_lb_port = var.listener_lb_port
  listen_lb_protocal = var.listen_lb_protocal
  listener_instance_port = var.listener_instance_port
  listener_instance_portocal = var.listener_instance_portocal
  asg_min_size = var.asg_min_size
  asg_desired_capacity = var.asg_desired_capacity
  asg_max_size = var.asg_max_size
  asg_key_name = var.asg_key_name
  asg_key_value = var.asg_key_value
  }
