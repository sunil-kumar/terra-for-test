#S3_Bucket Creation
resource "aws_s3_bucket" "my_s3" {
  bucket = var.bucketname
  acl    = var.acl
  tags = {
    Name        = "${var.keyname}-${var.project-name}"
    Environment = "${var.keyvalue}-${var.project-name}"
  }
}
#output
output "bucket_id" {
  value = aws_s3_bucket.my_s3.id
}
