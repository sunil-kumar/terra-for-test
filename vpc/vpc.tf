#VPC Creation
resource "aws_vpc" "myvpc" {
  cidr_block       = var.VPCCIDR
  instance_tenancy = "default"
  tags = {
    Name = "${var.vpcname}-${var.project-name}"
  }
}
#Subnet1 Creation
resource "aws_subnet" "mysub1" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = var.Subnet1CIDR
  availability_zone_id = var.subnet1Az
  tags = {
    Name = "${var.Subnet1name}-${var.project-name}"
  }
}
#Subnet2 Creation
resource "aws_subnet" "mysub2" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = var.Subnet2CIDR
  availability_zone_id = var.subnet2Az
  tags = {
    Name = "${var.Subnet2name}-${var.project-name}"
  }
}
#Route Table creation
resource "aws_route_table" "my_RT" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "${var.Routetablename}-${var.project-name}"
  }
}
#Subnet1 association
resource "aws_route_table_association" "subnet1" {
  subnet_id      = aws_subnet.mysub1.id
  route_table_id = aws_route_table.my_RT.id
}
#Subnet2 association
resource "aws_route_table_association" "subnet2" {
  subnet_id      = aws_subnet.mysub2.id
  route_table_id = aws_route_table.my_RT.id
}


#Security Group creation
resource "aws_security_group" "vpc_security_group" {
  name        = "${var.vpc_securitygroup_name}-${var.project-name}"
  description = "test_sec_grp2"
  vpc_id      = aws_vpc.myvpc.id

  ingress {
    description = "TLS from VPC"
    from_port   = var.ingress_vpc
    to_port     = var.ingress_vpc
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = var.egress_vpc
    to_port     = var.egress_vpc
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test_sec_grp2-${var.project-name}"
  }
}

#output
output "vpc_id" {
  value = aws_vpc.myvpc.id
}

output "subnet1_id" {
  value = aws_subnet.mysub1.id
}

output "subnet2_id" {
  value = aws_subnet.mysub2.id
}

output "route_table_id" {
  value = aws_route_table.my_RT.id
}

output "security_group_vpc" {
  value = aws_security_group.vpc_security_group.id
}
