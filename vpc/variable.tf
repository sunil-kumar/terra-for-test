#Main Variables
variable "VPCCIDR" {
  default = "10.10.0.0/16"
}
variable "vpcname" {
  default = "privatevpc"
}
variable "Subnet1CIDR" {
  default = "10.10.1.0/24"
}
variable "subnet1Az" {

}
variable "Subnet1name"{
  default = "privatesubnet1"
}
variable "Subnet2CIDR" {
  default = "10.10.2.0/24"
}
variable "subnet2Az" {

}
variable "Subnet2name"{
  default = "privatesubnet2"
}
variable "Routetablename" {
  default = "privateroutetable"
}
variable "vpc_securitygroup_name" {

}
variable "ingress_vpc" {

}
variable "egress_vpc" {

}
variable "project-name" {

}
